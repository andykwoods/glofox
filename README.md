# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Application Architecture ###

I am using MVP as the main architecture in the app. I am using the Command Pattern to completely modularise the API calls from the main application. 
Doing this would allow independent deployability of the API component. In fact wirh some more work, it could be separated out from the project into a Jar file. 

### API Issues ###

I had some issues with the API on Swagger, Postman and in my development. I was getting an `INVALID_BRANCH_ID` error. 
In order to run some testing with the app, I simply created a raw JSON file and return that from the GetMembersService. 
There is a boolean value to turn this off. 
An improvement would be to use a different build type in Gradle for this. 

### Tests ###

I have added business logic unit tests for the Presenter and Interactor. 
I have also unit tested the UI list row creation using a factory to set up the UI Structure (View Model) for the row items.

### Filtering ###

I haven't done the filtering for email, first name, last name etc. To be honest, I've not had the time to keep going. 
However, were I to do it, I would simply add a dropdown in the app bar. One selection of a filter, we would tell the Presenter, which would in turn inform the interactor
to make an API with the set parameter. 

### 3rd Party Libraries ###

I am using Retrofit and OkHttp for contacting the API.
I am using PowerMock and Mockito for testing. 

### Where is Dagger? ###

This project is a bit small for Dagger, which can add complication. However, it would be nice to Inject the interactor in the presenter instead of the way I have it done here. 

### RXAndroid? ###

RX is very powerful library. I am happy to use Intent Services and have a more pure application. However, RX could certainly be used instead of an Intent Service here. 

### Kotlin? ###

I haven't really had enough time with Kotlin as yet to use it for a tech test. 

### Messaging ###

I am using Broadcast Intent for returning the service response, however, EventBus is also another option. 

### NB ###

Please note this is developed using Android Studio 3.0. 

