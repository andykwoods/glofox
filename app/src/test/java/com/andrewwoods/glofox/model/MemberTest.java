package com.andrewwoods.glofox.model;

import com.andrewwoods.glofox.TestConstants;
import com.andrewwoods.glofox.api.gson.GlofoxGsonDeserialiser;
import com.andrewwoods.glofox.models.Member;
import com.google.gson.Gson;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MemberTest {

  @Test public void testGsonSerialisation() {
    Member member = new GlofoxGsonDeserialiser().fromJson(TestConstants.MEMBER_JSON_OBJECT, Member.class);
    assertEquals("56cddd2b5c46bb12a2b924ae", member.getId());
    assertEquals(true, member.getActive());
    assertEquals("56cdc0155c46bb176bb92582", member.getBranchId());
    assertEquals("test@zappy.ie", member.getEmail());
    assertEquals("Test", member.getFirstName());
    assertEquals("User", member.getLastName());
    assertEquals("thewodfactory", member.getNamespace());
    assertEquals("9999999999", member.getPhone());
    assertEquals("member", member.getType());
    assertEquals("Test User", member.getName());
    assertEquals(
        "https://s3-eu-west-1.amazonaws.com/glofox/staging/thewodfactory/branches/56cdc0155c46bb176bb92582/users/56cddd2b5c46bb12a2b924ae.png",
        member.getImageUrl());
  }
}
