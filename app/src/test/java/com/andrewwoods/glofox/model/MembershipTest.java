package com.andrewwoods.glofox.model;

import com.andrewwoods.glofox.TestConstants;
import com.andrewwoods.glofox.api.gson.GlofoxGsonDeserialiser;
import com.andrewwoods.glofox.models.Membership;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MembershipTest {

  private static final String API_RESPONSE = "{\n"
      + "                \"_id\": \"57ffdb86edc46090728b4567\",\n"
      + "                \"type\": \"time_classes\",\n"
      + "                \"start_date\": 1491177600,\n"
      + "                \"membership_group_id\": null,\n"
      + "                \"plan_code\": 1476385641150,\n"
      + "                \"boooked_events\": 0,\n"
      + "                \"expiry_date\": 1522713600\n"
      + "            }";


  @Test public void testGsonSerialisation() {
    Membership membership = new GlofoxGsonDeserialiser().fromJson(TestConstants.MEMBERSHIP_JSON_OBJECT, Membership.class);
    assertEquals("57ffdb86edc46090728b4567", membership.getId());
    assertEquals("time_classes", membership.getType());
    assertEquals(1491177600L, membership.getStartDate().longValue());
    assertEquals(null, membership.getMembershipGroupId());
    assertEquals(1476385641150L, membership.getPlanCode().longValue());
    assertEquals(0, membership.getBookedEvents().intValue());
    assertEquals(1522713600L, membership.getExpiryDate().longValue());
  }

}
