package com.andrewwoods.glofox.ui.adapter;

import android.content.res.Resources;
import com.andrewwoods.glofox.R;
import com.andrewwoods.glofox.models.Member;
import com.andrewwoods.glofox.models.Membership;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MemberUIStructureFactoryTest {

  @Mock private Resources resources;
  @Mock private Member member;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    when(resources.getString(anyInt(), Matchers.any())).thenReturn("");
    when(member.getFirstName()).thenReturn("Joe");
    when(member.getLastName()).thenReturn("Bloggs");
    Membership membership = mock(Membership.class);
    when(membership.getType()).thenReturn("time_classes");
    when(membership.getExpiryDate()).thenReturn(1522713600L);
    when(member.getMembership()).thenReturn(membership);

  }

  @Test
  public void testNullMember() {
    MemberUIStructure structure = MemberUIStructureFactory.buildMemberUIStructure(resources, null);
    //noinspection ConstantConditions
    assertEquals(null, structure);
  }

  @Test public void testMemberTitle() {
    MemberUIStructureFactory.buildMemberUIStructure(resources, member);
    verify(resources).getString(R.string.member_title_format, "Bloggs", "Joe");
  }

  @Test public void testMembershipType() {
    MemberUIStructure structure = MemberUIStructureFactory.buildMemberUIStructure(resources, member);
    assertEquals("Time classes", structure.membershipType);
  }

  @Test public void testMemberActive() {
    when(member.getActive()).thenReturn(true);
    MemberUIStructureFactory.buildMemberUIStructure(resources, member);
    verify(resources).getString(R.string.member_active);
  }

  @Test public void testMemberInactive() {
    when(member.getActive()).thenReturn(false);
    MemberUIStructureFactory.buildMemberUIStructure(resources, member);
    verify(resources).getString(R.string.member_inactive);
  }

  @Test public void testMemberExpiry() {
    MemberUIStructureFactory.buildMemberUIStructure(resources, member);
    verify(resources).getString(R.string.member_title_format, "Bloggs", "Joe");
    verify(resources).getString(R.string.member_expiry, "03-04-2018");
  }

}