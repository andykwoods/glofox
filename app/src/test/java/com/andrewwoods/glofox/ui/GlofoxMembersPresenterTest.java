package com.andrewwoods.glofox.ui;

import android.content.Context;
import android.test.mock.MockContext;
import com.andrewwoods.glofox.api.response.MembersResponseModel;
import com.andrewwoods.glofox.models.Member;
import com.andrewwoods.glofox.ui.interfaces.MembersInteractor;
import com.andrewwoods.glofox.ui.interfaces.MembersView;
import com.andrewwoods.glofox.util.Connectivity;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Connectivity.class})
public class GlofoxMembersPresenterTest {

  @Mock private MembersView view;

  @Mock private MembersInteractor interactor;

  private GlofoxMembersPresenter presenter;

  @Before
  public void testSetUp() {
    MockitoAnnotations.initMocks(this);
    presenter = new GlofoxMembersPresenter(null, view);
    presenter.interactor = interactor;
    presenter.requestPage = 1;
  }

  @Test
  public void testViewResumedConnectedAndGenresCacheNotReady() {
    mockStatic(Connectivity.class);
    PowerMockito.when(Connectivity.isConnected(any(Context.class))).thenReturn(true);
    presenter.viewResumed(new MockContext());
    verify(view).showLoading();
    verify(interactor).registerMembersReceiver();
    verify(interactor).requestMembers(eq(1));
  }

  @Test
  public void testViewResumedNotConnected() {
    mockStatic(Connectivity.class);
    PowerMockito.when(Connectivity.isConnected(any(Context.class))).thenReturn(false);
    presenter.viewResumed(new MockContext());
    verify(view).showLoading();
    verify(view).showNoConnection();
    verify(interactor).registerMembersReceiver();
    verify(interactor, never()).requestMembers(eq(1));
  }

  @Test
  public void testViewPaused() {
    presenter.viewPaused();
    verify(interactor).unregisterMembersReceiver();
  }

  @Test
  public void testSuccessResponse_NoMoreMembers() {
    List<Member> members = new ArrayList<>(1);
    Member member = mock(Member.class);
    members.add(member);
    MembersResponseModel responseModel = mock(MembersResponseModel.class);
    when(responseModel.getMembers()).thenReturn(members);
    when(responseModel.hasMore()).thenReturn(false);
    when(responseModel.getPage()).thenReturn(1);
    presenter.membersSuccessResponse(responseModel);
    verify(view).hideLoading();
    verify(view).hideRefreshSpinner();
    verify(view).loadMembers(members);
    assertFalse(presenter.hasMoreMembers);
  }

  @Test
  public void testSuccessResponse_HasMoreMembers() {
    List<Member> members = new ArrayList<>(1);
    Member member = mock(Member.class);
    members.add(member);
    MembersResponseModel responseModel = mock(MembersResponseModel.class);
    when(responseModel.getMembers()).thenReturn(members);
    when(responseModel.hasMore()).thenReturn(true);
    when(responseModel.getPage()).thenReturn(1);
    presenter.membersSuccessResponse(responseModel);
    verify(view).hideLoading();
    verify(view).hideRefreshSpinner();
    verify(view).loadMembers(members);
    assertTrue(presenter.hasMoreMembers);
  }

  @Test
  public void testSuccessResponse_PageGreaterThanOne() {
    List<Member> members = new ArrayList<>(1);
    Member member = mock(Member.class);
    members.add(member);
    MembersResponseModel responseModel = mock(MembersResponseModel.class);
    when(responseModel.getMembers()).thenReturn(members);
    when(responseModel.hasMore()).thenReturn(true);
    when(responseModel.getPage()).thenReturn(2);
    presenter.membersSuccessResponse(responseModel);
    verify(view).hideLoading();
    verify(view).hideRefreshSpinner();
    verify(view).addMembers(members);
    assertTrue(presenter.hasMoreMembers);
  }

  @Test
  public void testMembersErrorResponse() {
    presenter.membersErrorResponse();
    verify(view).hideRefreshSpinner();
    verify(view).showNoMembers();
  }

  @Test
  public void testRefreshMembers() {
    presenter.refreshMembers();
    verify(view).showRefreshSpinner();
    verify(interactor).requestMembers(eq(1));
  }

  @Test
  public void testOnBottomReached_HasMoreMembers() {
    presenter.hasMoreMembers = true;
    presenter.onBottomReached();
    verify(interactor).requestMembers(eq(2));
  }

  @Test
  public void testOnBottomReached_NoMoreMembers() {
    presenter.hasMoreMembers = false;
    presenter.onBottomReached();
    verify(interactor, never()).requestMembers(eq(2));
  }
}