package com.andrewwoods.glofox.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.test.mock.MockContext;
import com.andrewwoods.glofox.api.ResponseCodes;
import com.andrewwoods.glofox.api.response.MembersResponseModel;
import com.andrewwoods.glofox.common.Extras;
import com.andrewwoods.glofox.ui.interfaces.MembersPresenter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.support.SuppressCode.suppressConstructor;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocalBroadcastManager.class})
public class GlofoxMembersInteractorTest {

  @Mock private MembersPresenter presenter;

  private GlofoxMembersInteractor membersInteractor;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    membersInteractor = new GlofoxMembersInteractor(new MockContext(), presenter);
  }

  @Test
  public void testRegisterMembersReceiver() {
    LocalBroadcastManager manager = mockBroadcastManager();
    membersInteractor.registerMembersReceiver();
    verify(manager).registerReceiver(any(BroadcastReceiver.class), any(IntentFilter.class));
  }

  @Test
  public void testUnregisterMembersReceiver() {
    LocalBroadcastManager manager = mockBroadcastManager();
    membersInteractor.unregisterMembersReceiver();
    verify(manager).unregisterReceiver(any(BroadcastReceiver.class));
  }

  @Test
  public void testMembersSuccessResponse() {
    MembersResponseModel responseModel = mock(MembersResponseModel.class);
    Intent intent = mock(Intent.class);
    when(intent.getIntExtra(Extras.RESPONSE_CODE, -1)).thenReturn(ResponseCodes.OK);
    when(intent.getParcelableExtra(Extras.PARAM_MEMBERS_RESPONSE)).thenReturn(responseModel);
    membersInteractor.membersReceiver.onReceive(new MockContext(), intent);
    verify(presenter).membersSuccessResponse(responseModel);
  }

  @Test
  public void testMoviesErrorResponse() {
    Intent intent = mock(Intent.class);
    when(intent.getIntExtra(Extras.RESPONSE_CODE, -1)).thenReturn(ResponseCodes.NETWORK_ERROR);
    membersInteractor.membersReceiver.onReceive(new MockContext(), intent);
    verify(presenter).membersErrorResponse();
  }

  private LocalBroadcastManager mockBroadcastManager() {
    suppressConstructor(LocalBroadcastManager.class);
    mockStatic(LocalBroadcastManager.class);
    LocalBroadcastManager manager = PowerMockito.mock(LocalBroadcastManager.class);
    when(LocalBroadcastManager.getInstance(any(Context.class))).thenReturn(manager);
    return manager;
  }

}