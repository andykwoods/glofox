package com.andrewwoods.glofox.api.response;

import com.andrewwoods.glofox.api.gson.GlofoxGsonDeserialiser;
import org.junit.Test;

import static com.andrewwoods.glofox.TestConstants.MEMBERS_RESPONSE_JSON_OBJECT;
import static org.junit.Assert.assertEquals;

public class MembersResponseModelTest {

  @Test public void testGsonSerialisation() {
    MembersResponseModel responseModel = new GlofoxGsonDeserialiser().fromJson(MEMBERS_RESPONSE_JSON_OBJECT, MembersResponseModel.class);
    assertEquals(1, responseModel.getPage().intValue());
    assertEquals(50, responseModel.getLimit().intValue());
    assertEquals(false, responseModel.hasMore());
    assertEquals(1, responseModel.getTotalCount().intValue());
    assertEquals(1, responseModel.getMembers().size());
  }
}