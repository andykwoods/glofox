package com.andrewwoods.glofox;

public class TestConstants {


  public static final String MEMBERSHIP_JSON_OBJECT = "{\n"
      + "                \"_id\": \"57ffdb86edc46090728b4567\",\n"
      + "                \"type\": \"time_classes\",\n"
      + "                \"start_date\": 1491177600,\n"
      + "                \"membership_group_id\": null,\n"
      + "                \"plan_code\": 1476385641150,\n"
      + "                \"boooked_events\": 0,\n"
      + "                \"expiry_date\": 1522713600\n"
      + "            }";

  public static final String MEMBER_JSON_OBJECT = " {\n"
      + "            \"_id\": \"56cddd2b5c46bb12a2b924ae\",\n"
      + "            \"active\": true,\n"
      + "            \"branch_id\": \"56cdc0155c46bb176bb92582\",\n"
      + "            \"email\": \"test@zappy.ie\",\n"
      + "            \"first_name\": \"Test\",\n"
      + "            \"last_name\": \"User\",\n"
      + "            \"membership\": "+ MEMBERSHIP_JSON_OBJECT+",\n"
      + "            \"namespace\": \"thewodfactory\",\n"
      + "            \"phone\": \"9999999999\",\n"
      + "            \"type\": \"member\",\n"
      + "            \"name\": \"Test User\",\n"
      + "            \"image_url\": \"https://s3-eu-west-1.amazonaws.com/glofox/staging/thewodfactory/branches/56cdc0155c46bb176bb92582/users/56cddd2b5c46bb12a2b924ae.png\"\n"
      + "        }";

  public static final String MEMBERS_RESPONSE_JSON_OBJECT = "{\n"
      + "    \"object\": \"list\",\n"
      + "    \"page\": 1,\n"
      + "    \"limit\": 50,\n"
      + "    \"has_more\": false,\n"
      + "    \"total_count\": 1,\n"
      + "    \"data\": [\n"
      +       MEMBER_JSON_OBJECT
      + "    ]\n"
      + "}";

}
