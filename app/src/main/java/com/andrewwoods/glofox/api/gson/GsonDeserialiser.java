package com.andrewwoods.glofox.api.gson;

public interface GsonDeserialiser {
  <T> T fromJson(String json, Class<T> clazz);
}
