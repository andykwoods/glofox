package com.andrewwoods.glofox.api.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;
import com.andrewwoods.glofox.R;
import com.andrewwoods.glofox.api.gson.GlofoxGsonDeserialiser;
import com.andrewwoods.glofox.api.retrofit.ServiceApi;
import com.andrewwoods.glofox.api.response.MembersResponseModel;
import com.andrewwoods.glofox.api.retrofit.GlofoxRetrofitFactory;
import com.andrewwoods.glofox.api.retrofit.RetrofitService;
import com.andrewwoods.glofox.api.ResponseCodes;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import retrofit2.Call;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.andrewwoods.glofox.common.Extras.PARAM_MEMBERS_RESPONSE;
import static com.andrewwoods.glofox.common.Extras.REQUEST_PAGE;
import static com.andrewwoods.glofox.common.Extras.RESPONSE_CODE;
import static com.andrewwoods.glofox.common.Extras.RESULT_RECEIVER;

public class GetMembersService extends IntentService {

  private ResultReceiver resultReceiver;

  public static void start(Context context, int page, ResultReceiver resultReceiver) {
    if (context == null) return;
    Intent intent = new Intent(context, GetMembersService.class);
    intent.putExtra(REQUEST_PAGE, page);
    intent.putExtra(RESULT_RECEIVER, resultReceiver);
    context.startService(intent);
  }

  @SuppressWarnings("unused") public GetMembersService(String name) {
    super(name);
  }

  @SuppressWarnings("unused")
  public GetMembersService() {
    super("com.andrewwoods.glofox.api.services.GetMembersService");
  }

  @Override protected void onHandleIntent(@Nullable Intent intent) {
    resultReceiver =
        intent == null ? null : (ResultReceiver) intent.getParcelableExtra(RESULT_RECEIVER);
    int page = intent == null ? 1 : intent.getIntExtra(REQUEST_PAGE, 1);
    final MembersResponseModel responseModel = requestMembers(page);
    handleResponse(responseModel);
  }

  public MembersResponseModel requestMembers(int page) {
    if (getResources().getBoolean(R.bool.use_api_call)) {
      return requestResponseFromApi(page);
    } else {
      return buildResponseModelFromRawFile(page);
    }
  }

  @Nullable private MembersResponseModel requestResponseFromApi(int page) {
    Call<MembersResponseModel> membersResponseModelCall = getRetrofitService(this).requestMembers(page);

    MembersResponseModel responseModel = null;
    try {
      Response<MembersResponseModel> membersResponse = membersResponseModelCall.execute();

      if (isRequestSuccessful(membersResponse)){
        responseModel = membersResponse.body();
      } else {
        logRetrofitError(TAG, membersResponse);
        notifyListener(ResponseCodes.NETWORK_ERROR, null);
      }
    } catch (IOException e) {
      Log.e(TAG, e.getMessage(), e);
      notifyListener(ResponseCodes.NETWORK_ERROR, null);
    }
    return responseModel;
  }

  private MembersResponseModel buildResponseModelFromRawFile(int page) {
    Writer writer = new StringWriter();
    char[] buffer = new char[1024];
    try (InputStream is = getResources().openRawResource(R.raw.response)) {
      Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
      int n;
      while ((n = reader.read(buffer)) != -1) {
        writer.write(buffer, 0, n);
      }
    } catch (Exception ignore) {
    }

    String jsonString = writer.toString();
    MembersResponseModel responseModel =
        new GlofoxGsonDeserialiser().fromJson(jsonString, MembersResponseModel.class);
    responseModel.setPage(page);
    return responseModel;
  }

  public ServiceApi getRetrofitService(Context context) {
    final RetrofitService
        retrofitService = new RetrofitService(new GlofoxRetrofitFactory(context));
    return retrofitService.createService(ServiceApi.class);
  }

  public boolean isRequestSuccessful(Response response) {
    return response.isSuccessful() && response.errorBody() == null;
  }

  public static void logRetrofitError(String tag, Response response) {
    try {
      Log.e(tag, response.errorBody().string());
    } catch (IOException e) {
      Log.e(TAG, e.getMessage(), e);
    }
  }

  private void handleResponse(MembersResponseModel responseModel) {
    if (responseModel != null)
      notifyListener(ResponseCodes.OK, responseModel);
    else
      notifyListener(ResponseCodes.NETWORK_ERROR, null);
  }

  private void notifyListener(int responseCode, MembersResponseModel responseModel) {
    Bundle bundle = new Bundle(2);
    bundle.putInt(RESPONSE_CODE, responseCode);
    bundle.putParcelable(PARAM_MEMBERS_RESPONSE, responseModel);
    if (resultReceiver != null) resultReceiver.send(responseCode, bundle);
  }
}
