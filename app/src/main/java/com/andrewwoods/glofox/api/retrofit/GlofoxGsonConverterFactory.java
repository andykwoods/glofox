package com.andrewwoods.glofox.api.retrofit;

import com.andrewwoods.glofox.api.gson.GlofoxGsonDeserialiser;
import retrofit2.converter.gson.GsonConverterFactory;

public class GlofoxGsonConverterFactory {

  public static GsonConverterFactory create() {
    return GsonConverterFactory.create(GlofoxGsonDeserialiser.getGsonInstance());
  }
}
