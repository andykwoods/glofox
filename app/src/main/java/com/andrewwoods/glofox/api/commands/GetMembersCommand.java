package com.andrewwoods.glofox.api.commands;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.content.LocalBroadcastManager;
import com.andrewwoods.glofox.api.BroadcastActions;
import com.andrewwoods.glofox.api.interfaces.Commander;
import com.andrewwoods.glofox.api.services.GetMembersService;

public class GetMembersCommand implements Commander {

  private final Context context;
  private final int page;
  private static final Handler handler = new Handler();

  public GetMembersCommand(Context context, int page) {
    this.context = context;
    this.page = page;
  }

  @Override public void execute() {
    GetMembersService.start(context, page, new CommandReceiver(handler));
  }

  private class CommandReceiver extends ResultReceiver {

    CommandReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      super.onReceiveResult(resultCode, resultData);
      sendBroadcastResult(resultData);
    }
  }

  private void sendBroadcastResult(Bundle extras) {
    Intent intent = new Intent(BroadcastActions.BROADCAST_ACTION_MEMBERS);
    intent.putExtras(extras);
    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
  }
}
