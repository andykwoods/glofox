package com.andrewwoods.glofox.api.gson;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class GlofoxGsonDeserialiser implements GsonDeserialiser {

  private static Gson sGson;

  static {
    sGson = new GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create();
  }

  @Override
  public <T> T fromJson(String json, Class<T> clazz) {
    try {
      return sGson.fromJson(json, clazz);
    } catch (JsonSyntaxException e) {
      throw new JsonSyntaxException(json + "." + e.getMessage(), e);
    }

  }

  public static Gson getGsonInstance() {
    return sGson;
  }
}
