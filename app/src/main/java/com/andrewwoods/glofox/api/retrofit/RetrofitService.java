package com.andrewwoods.glofox.api.retrofit;

import retrofit2.Retrofit;

public class RetrofitService {

    private RetrofitFactory retrofitFactory;
    public static Retrofit retrofit;

    public RetrofitService(RetrofitFactory retrofitFactory) {
        setRetrofitFactory(retrofitFactory);
    }

    public void setRetrofitFactory(RetrofitFactory factory) {
        retrofitFactory = factory;
    }

    public <T> T createService(Class<T> serviceClass) {
        retrofit = retrofitFactory.createRetrofitBuilder().build();
        return retrofit.create(serviceClass);
    }
}
