package com.andrewwoods.glofox.api.retrofit;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public class GlofoxConverterFactory extends Converter.Factory {
    private final Converter.Factory gsonConverterFactory = GlofoxGsonConverterFactory.create();

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter (Type type, Annotation[] annotations,
                                                             Retrofit retrofit) {
        return gsonConverterFactory.responseBodyConverter(type, annotations, retrofit);
    }
}
