package com.andrewwoods.glofox.api.retrofit;

import com.andrewwoods.glofox.api.response.MembersResponseModel;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceApi {

  @POST("members")
  Call<MembersResponseModel> requestMembers(@Query("page") int page);
}
