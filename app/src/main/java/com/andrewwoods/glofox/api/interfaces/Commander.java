package com.andrewwoods.glofox.api.interfaces;

public interface Commander {

  void execute();
}
