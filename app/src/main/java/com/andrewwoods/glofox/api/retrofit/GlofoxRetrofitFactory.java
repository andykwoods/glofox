package com.andrewwoods.glofox.api.retrofit;

import android.content.Context;
import android.util.Log;
import com.andrewwoods.glofox.R;
import com.andrewwoods.glofox.common.Constants;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

public class GlofoxRetrofitFactory implements RetrofitFactory {

  private static final String TAG = "GlofoxRetrofitFactory";
  private Context context;
  private String apiKey;

  public GlofoxRetrofitFactory(Context context) {
    this.context = context;
    apiKey = context.getString(R.string.api_key);
  }

  @Override public Retrofit.Builder createRetrofitBuilder() {
    return getRetrofitBuilder();
  }

  private Retrofit.Builder getRetrofitBuilder() {
    String baseUrl = context.getString(R.string.base_url);
    Log.v(TAG, " BaseUrl: " + baseUrl);
    return new Retrofit.Builder().baseUrl(baseUrl)
        .addConverterFactory(new GlofoxConverterFactory())
        .client(buildOkHttpClient());
  }

  private OkHttpClient buildOkHttpClient() {
    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    httpClient.addInterceptor(buildLoggingInterceptor());
    httpClient.readTimeout(context.getResources().getInteger(R.integer.timeout_read), TimeUnit.SECONDS);

    httpClient.addInterceptor(new Interceptor() {
      @Override public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder();
        requestBuilder.addHeader(Constants.AUTHORIZATION, "Bearer " + apiKey);
        Request request = requestBuilder.build();
        return chain.proceed(request);
      }
    });
    return httpClient.build();
  }

  private Interceptor buildLoggingInterceptor() {
    HttpLoggingInterceptor loggingInterceptor =
        new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
          @Override public void log(String message) {
            Log.d(TAG, message);
          }
        });
    loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return loggingInterceptor;
  }
}