package com.andrewwoods.glofox.api.retrofit;


import retrofit2.Retrofit;

interface RetrofitFactory {

    Retrofit.Builder createRetrofitBuilder();
}
