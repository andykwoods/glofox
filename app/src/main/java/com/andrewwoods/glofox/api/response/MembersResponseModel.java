package com.andrewwoods.glofox.api.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.andrewwoods.glofox.models.Member;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class MembersResponseModel implements Parcelable {

  private Integer page;
  private Integer limit;
  private Boolean hasMore;
  private Integer totalCount;
  @SerializedName("data")
  private List<Member> members;

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  Integer getLimit() {
    return limit;
  }

  public Boolean hasMore() {
    return hasMore;
  }

  Integer getTotalCount() {
    return totalCount;
  }

  public List<Member> getMembers() {
    return members;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.page);
    dest.writeValue(this.limit);
    dest.writeValue(this.hasMore);
    dest.writeValue(this.totalCount);
    dest.writeList(this.members);
  }

  public MembersResponseModel() {
  }

  protected MembersResponseModel(Parcel in) {
    this.page = (Integer) in.readValue(Integer.class.getClassLoader());
    this.limit = (Integer) in.readValue(Integer.class.getClassLoader());
    this.hasMore = (Boolean) in.readValue(Boolean.class.getClassLoader());
    this.totalCount = (Integer) in.readValue(Integer.class.getClassLoader());
    this.members = new ArrayList<>();
    in.readList(this.members, Member.class.getClassLoader());
  }

  public static final Parcelable.Creator<MembersResponseModel> CREATOR =
      new Parcelable.Creator<MembersResponseModel>() {
        @Override public MembersResponseModel createFromParcel(Parcel source) {
          return new MembersResponseModel(source);
        }

        @Override public MembersResponseModel[] newArray(int size) {
          return new MembersResponseModel[size];
        }
      };
}
