package com.andrewwoods.glofox.api;

@SuppressWarnings("unused")
public class ResponseCodes {

	public static final int OK 						=	0;
	public static final int NETWORK_ERROR	=	1;
}
