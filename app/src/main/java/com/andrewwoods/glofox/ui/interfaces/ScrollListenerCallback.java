package com.andrewwoods.glofox.ui.interfaces;

public interface ScrollListenerCallback {

    void onBottomReached();
}
