package com.andrewwoods.glofox.ui.widget;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import com.andrewwoods.glofox.ui.interfaces.ScrollListenerCallback;

public class LinearRecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private int position;
    private int totalLoadedCount;

    private final float percentageOfPageForOnBottom = 0.9f;

    private int lastFirstVisible = -1;
    private int lastVisibleCount = -1;
    private int lastItemCount = -1;
    private ScrollListenerCallback callback;

    public LinearRecyclerViewScrollListener(ScrollListenerCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

        int firstVisible = layoutManager.findFirstVisibleItemPosition();
        int visibleCount = Math.abs(firstVisible - layoutManager.findLastVisibleItemPosition());
        int itemCount = recyclerView.getAdapter().getItemCount();

        if (hasScrollChanged(firstVisible, visibleCount, itemCount)) {

            handleOnScroll(firstVisible, visibleCount, itemCount);

            lastFirstVisible = firstVisible;
            lastVisibleCount = visibleCount;
            lastItemCount = itemCount;
        }
    }

    private boolean hasScrollChanged(int firstVisible, int visibleCount, int itemCount) {
        return firstVisible != lastFirstVisible || visibleCount != lastVisibleCount
                || itemCount != lastItemCount;
    }

    private void handleOnScroll(int firstVisibleItem, int visibleItemCount,
        int totalItemCount) {
        if (firstVisibleItem != 0) {
            checkIsBottomReached(firstVisibleItem, visibleItemCount, totalItemCount);
        }
        position = firstVisibleItem;

    }

    private void checkIsBottomReached(int firstVisibleItem, int visibleItemCount,
        int totalItemCount) {
        if (position <= firstVisibleItem) {

            if (isBottomListReached(firstVisibleItem, visibleItemCount, totalItemCount, totalLoadedCount, percentageOfPageForOnBottom)) {
                setTotalLoadedCount(totalItemCount);
                if (callback != null) callback.onBottomReached();
            }
        }
    }

    private boolean isBottomListReached(int firstVisibleItem, int visibleItemCount,
        int totalItemCount, int totalLoadedCount, float percentage) {
        return totalItemCount != totalLoadedCount
            && firstVisibleItem + visibleItemCount >= (totalItemCount * percentage);
    }

    private void setTotalLoadedCount(int totalItemCount) {
        totalLoadedCount = totalItemCount;
    }

    public void resetLoadedCount() {
        setTotalLoadedCount(0);
    }
}
