package com.andrewwoods.glofox.ui.adapter;

import android.content.res.Resources;
import com.andrewwoods.glofox.R;
import com.andrewwoods.glofox.models.Member;
import com.andrewwoods.glofox.models.Membership;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class MemberUIStructureFactory {

    private static final String DATE_FORMAT = "dd-MM-yyyy";

    static MemberUIStructure buildMemberUIStructure(Resources resources, Member member) {
        if (member == null)
            return null;
        MemberUIStructure structure = new MemberUIStructure();
        buildMemberTitle(resources, structure, member);
        buildMembershipType(structure, member);
        buildMemberActiveStatus(resources, structure, member);
        buildExpDate(resources, structure, member);
        return structure;
    }

    private static void buildMemberTitle(Resources resources, MemberUIStructure structure, Member member) {
        structure.memberTitle = resources.getString(R.string.member_title_format, member.getLastName(),
            member.getFirstName());
    }

    private static void buildMembershipType(MemberUIStructure structure, Member member) {
        Membership membership = member.getMembership();
        structure.membershipType = membership == null ? ""
        : getMembershipType(membership);
    }

    private static void buildMemberActiveStatus(Resources resources, MemberUIStructure structure, Member member) {
        if (member.getActive() != null && member.getActive())
            structure.memberActiveStatus = resources.getString(R.string.member_active);
        else
            structure.memberActiveStatus = resources.getString(R.string.member_inactive);
    }

    private static void buildExpDate(Resources resources, MemberUIStructure structure,
        Member member) {
        if (member.getMembership() == null)
            structure.memberExpiry = "";
        else {
            DateFormat format = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
            Date date = new Date(member.getMembership().getExpiryDate() * 1000);
            structure.memberExpiry = resources.getString(R.string.member_expiry, format.format(date));
        }
    }

    private static String getMembershipType(Membership membership) {
        String type = membership.getType();
        if (type == null || type.isEmpty()) {
            return "";
        } else {
            type = type.replaceAll("_", " ");
            return type.substring(0, 1).toUpperCase() + type.substring(1);
        }
    }

    //private static String buildMoviePopularity(Movie movie) {
    //    return movie.popularity == null ? "0" :
    //            String.format(Locale.ENGLISH, "%.0f", movie.popularity);
    //}
    //
    //private static String buildYearRelease(Movie movie) {
    //    if (movie.releaseDate == null) return "";
    //    try {
    //        DateFormat format = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
    //        Date date = format.parse(movie.releaseDate);
    //        Calendar calendar = Calendar.getInstance();
    //        calendar.setTime(date);
    //        return Integer.toString(calendar.get(Calendar.YEAR));
    //    } catch (ParseException ex) {
    //        String[] dateParts = movie.releaseDate.split("-");
    //        return dateParts[0];
    //    }
    //}
    //
    //private static String buildMovieGenre(Movie movie) {
    //    if (movie.genreIds == null) return "";
    //    Genre mainGenre = GenresCache.getInstance().getGenreById(movie.genreIds.get(0));
    //    if (mainGenre == null) return "";
    //    else return mainGenre.name;
    //}
}
