package com.andrewwoods.glofox.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import com.andrewwoods.glofox.api.commands.GetMembersCommand;
import com.andrewwoods.glofox.api.response.MembersResponseModel;
import com.andrewwoods.glofox.ui.interfaces.MembersInteractor;
import com.andrewwoods.glofox.ui.interfaces.MembersPresenter;
import com.andrewwoods.glofox.api.BroadcastActions;
import com.andrewwoods.glofox.common.Extras;
import com.andrewwoods.glofox.api.ResponseCodes;

class GlofoxMembersInteractor implements MembersInteractor {

  private final Context context;
  private final MembersPresenter presenter;

  GlofoxMembersInteractor(Context context, MembersPresenter membersPresenter) {
    this.context = context;
    presenter = membersPresenter;
  }

  @Override public void registerMembersReceiver() {
    LocalBroadcastManager.getInstance(context)
        .registerReceiver(membersReceiver,
            new IntentFilter(BroadcastActions.BROADCAST_ACTION_MEMBERS));
  }

  @Override public void unregisterMembersReceiver() {
    LocalBroadcastManager.getInstance(context).unregisterReceiver(membersReceiver);
  }

  final BroadcastReceiver membersReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
      int response = intent.getIntExtra(Extras.RESPONSE_CODE, -1);
      if (response == ResponseCodes.OK) {
        MembersResponseModel members = intent.getParcelableExtra(Extras.PARAM_MEMBERS_RESPONSE);
        presenter.membersSuccessResponse(members);
      } else {
        presenter.membersErrorResponse();
      }
    }
  };

  @Override public void requestMembers(int page) {
    GetMembersCommand command = new GetMembersCommand(context, page);
    command.execute();
  }
}
