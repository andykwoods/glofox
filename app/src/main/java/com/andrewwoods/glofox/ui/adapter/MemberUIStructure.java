package com.andrewwoods.glofox.ui.adapter;

public class MemberUIStructure {

    public String memberThumbnailUrl;
    public String memberTitle;
    public String membershipType;
    public String memberActiveStatus;
    public String memberExpiry;
}
