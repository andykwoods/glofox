package com.andrewwoods.glofox.ui.interfaces;

import android.content.Context;
import com.andrewwoods.glofox.api.response.MembersResponseModel;

public interface MembersPresenter {

    void viewResumed(Context context);

    void viewPaused();

    void refreshMembers();

    void membersSuccessResponse(MembersResponseModel responseModel);

    void membersErrorResponse();

    void onBottomReached();
}
