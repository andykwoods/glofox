package com.andrewwoods.glofox.ui;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import com.andrewwoods.glofox.R;
import com.andrewwoods.glofox.models.Member;
import com.andrewwoods.glofox.ui.interfaces.MembersPresenter;
import com.andrewwoods.glofox.ui.interfaces.MembersView;
import com.andrewwoods.glofox.ui.adapter.MembersAdapter;
import com.andrewwoods.glofox.ui.interfaces.ScrollListenerCallback;
import com.andrewwoods.glofox.ui.widget.LinearRecyclerViewScrollListener;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MembersView, ScrollListenerCallback {

    private MembersPresenter presenter;
    private RecyclerView recyclerView;
    private MembersAdapter adapter;
    private View loadingView;
    private ViewStub noConnectionStub;
    private ViewStub noResultsStub;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View noResultsView;
    private LinearRecyclerViewScrollListener listViewScrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadingView = findViewById(R.id.members_progress);
        setUpList();
        noConnectionStub = findViewById(R.id.members_no_connection);
        noResultsStub = findViewById(R.id.members_no_results);
        swipeRefreshLayout = findViewById(R.id.members_swipe_refresh_layout);

        setPresenter(new GlofoxMembersPresenter(this, this));
        swipeRefreshLayout.setOnRefreshListener(swipeRefreshListener);
    }

    private void setUpList() {
        listViewScrollListener = new LinearRecyclerViewScrollListener(this);

        recyclerView = findViewById(R.id.members_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(buildItemDecoration());

        recyclerView.addOnScrollListener(listViewScrollListener);
    }

    private RecyclerView.ItemDecoration buildItemDecoration() {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(this,
                R.drawable.members_list_divider));
        return dividerItemDecoration;
    }

    private void setPresenter(MembersPresenter membersPresenter) {
        presenter = membersPresenter;
    }

    private final SwipeRefreshLayout.OnRefreshListener swipeRefreshListener
            = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            presenter.refreshMembers();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.member_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.menu_refresh:
                presenter.refreshMembers();
                return true;
        }
        return false;
    }
    @Override
    protected void onResume() {
        super.onResume();
        presenter.viewResumed(this);
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void loadMembers(List<Member> members) {
        if (adapter != null) {
            adapter.replaceList(members);
            resetAdapterIfNeeded();
        } else {
            buildNewAdapter(members);
        }
    }

    @Override public void addMembers(List<Member> members) {
        if (adapter != null) {
            adapter.addList(members);
            resetAdapterIfNeeded();
        } else {
            buildNewAdapter(members);
        }
    }

    private void resetAdapterIfNeeded() {
        if (recyclerView.getAdapter() == null) recyclerView.setAdapter(adapter);
    }

    private void buildNewAdapter(List<Member> members) {
        adapter = new MembersAdapter(this, members);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showNoMembers() {
        if (noResultsView == null)
            noResultsView = noResultsStub.inflate();
        else
            noResultsView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNoConnection() {
        noConnectionStub.inflate();
    }

    @Override
    public void showRefreshSpinner() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideRefreshSpinner() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override public void resetScrollListener() {
        listViewScrollListener.resetLoadedCount();
    }

    @Override public void onBottomReached() {
        presenter.onBottomReached();
    }
}
