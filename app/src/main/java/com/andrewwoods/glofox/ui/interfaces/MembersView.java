package com.andrewwoods.glofox.ui.interfaces;

import com.andrewwoods.glofox.models.Member;
import java.util.List;

public interface MembersView {

    void showLoading();

    void hideLoading();

    void loadMembers(List<Member> members);

    void addMembers(List<Member> members);

    void showNoMembers();

    void showNoConnection();

    void showRefreshSpinner();

    void hideRefreshSpinner();

    void resetScrollListener();
}
