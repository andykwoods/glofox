package com.andrewwoods.glofox.ui.interfaces;

public interface MembersInteractor {

    void registerMembersReceiver();

    void unregisterMembersReceiver();

    void requestMembers(int page);
}
