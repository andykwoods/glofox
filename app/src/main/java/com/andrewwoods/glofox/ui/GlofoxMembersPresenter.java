package com.andrewwoods.glofox.ui;

import android.content.Context;
import android.support.annotation.VisibleForTesting;
import com.andrewwoods.glofox.api.response.MembersResponseModel;
import com.andrewwoods.glofox.ui.interfaces.MembersInteractor;
import com.andrewwoods.glofox.ui.interfaces.MembersPresenter;
import com.andrewwoods.glofox.ui.interfaces.MembersView;
import com.andrewwoods.glofox.util.Connectivity;

public class GlofoxMembersPresenter implements MembersPresenter {

    @VisibleForTesting int requestPage = 1;
    private final MembersView view;
    @VisibleForTesting MembersInteractor interactor;
    @VisibleForTesting boolean hasMoreMembers;

    GlofoxMembersPresenter(Context context, MembersView view) {
        this.view = view;
        this.interactor = new GlofoxMembersInteractor(context, this);
    }

    @Override
    public void viewResumed(Context context) {
        view.showLoading();
        interactor.registerMembersReceiver();
        if (Connectivity.isConnected(context)) {
            interactor.requestMembers(requestPage);
        } else {
            view.showNoConnection();
        }
    }

    @Override
    public void viewPaused() {
        interactor.unregisterMembersReceiver();
    }

    @Override
    public void refreshMembers() {
        requestPage = 1;
        view.showRefreshSpinner();
        interactor.requestMembers(requestPage);
    }

    @Override
    public void membersSuccessResponse(MembersResponseModel responseModel) {
        hasMoreMembers = responseModel.hasMore();
        view.hideLoading();
        view.hideRefreshSpinner();
        if (responseModel.getPage() == 1)
            view.loadMembers(responseModel.getMembers());
        else
            view.addMembers(responseModel.getMembers());
    }

    @Override
    public void membersErrorResponse() {
        handleErrorResponse();
    }

    private void handleErrorResponse() {
        view.hideRefreshSpinner();
        view.showNoMembers();
    }

    @Override public void onBottomReached() {
        if (hasMoreMembers) {
            interactor.requestMembers(++requestPage);
        }
    }
}
