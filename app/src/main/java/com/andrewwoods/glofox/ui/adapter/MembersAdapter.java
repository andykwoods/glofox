package com.andrewwoods.glofox.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.andrewwoods.glofox.models.Member;
import com.andrewwoods.glofox.R;
import com.squareup.picasso.Picasso;
import java.util.List;

public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.MembersHolder> {

    private final Context context;
    private final Resources resources;
    private List<Member> items;
    private final LayoutInflater inflater;

    public MembersAdapter(Context context, List<Member> items) {
        this.context = context;
        this.resources = context.getResources();
        this.items = items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MembersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = inflater.inflate(R.layout.members_row_layout, parent, false);
        return new MembersHolder(view);
    }

    @Override
    public void onBindViewHolder(MembersHolder holder, int position) {
        final Member item = items.get(position);
        final MemberUIStructure
            structure = MemberUIStructureFactory.buildMemberUIStructure(resources, item);
        Picasso.with(context).load(structure.memberThumbnailUrl).placeholder(R.mipmap.ic_launcher)
            .into(holder.memberThumbnail);
        holder.memberTitle.setText(structure.memberTitle);
        holder.membershipType.setText(structure.membershipType);
        holder.memberActiveStatus.setText(structure.memberActiveStatus);
        holder.memberExpiry.setText(structure.memberExpiry);
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void replaceList(List<Member> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void addList(List<Member> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    class MembersHolder extends RecyclerView.ViewHolder {

        View memberLayout;
        ImageView memberThumbnail;
        TextView memberTitle;
        TextView membershipType;
        TextView memberActiveStatus;
        TextView memberExpiry;

        MembersHolder(View itemView) {
            super(itemView);
            memberLayout = itemView;
            memberThumbnail = itemView.findViewById(R.id.member_thumbnail);
            memberTitle = itemView.findViewById(R.id.member_title);
            membershipType = itemView.findViewById(R.id.membership_type);
            memberActiveStatus = itemView.findViewById(R.id.member_active_status);
            memberExpiry = itemView.findViewById(R.id.member_expiry);
        }
    }
}
