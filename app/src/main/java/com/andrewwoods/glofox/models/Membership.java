package com.andrewwoods.glofox.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class Membership implements Parcelable {

  @SerializedName("_id") private String id;
  private String type;
  private Long startDate;
  private String membershipGroupId;
  private Long planCode;
  @SerializedName("boooked_events") private Integer bookedEvents;
  private Long expiryDate;

  public String getId() {
    return id;
  }

  public String getType() {
    return type;
  }

  public Long getStartDate() {
    return startDate;
  }

  public String getMembershipGroupId() {
    return membershipGroupId;
  }

  public Long getPlanCode() {
    return planCode;
  }

  public Integer getBookedEvents() {
    return bookedEvents;
  }

  public Long getExpiryDate() {
    return expiryDate;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.id);
    dest.writeString(this.type);
    dest.writeValue(this.startDate);
    dest.writeString(this.membershipGroupId);
    dest.writeValue(this.planCode);
    dest.writeValue(this.bookedEvents);
    dest.writeValue(this.expiryDate);
  }

  public Membership() {
  }

  protected Membership(Parcel in) {
    this.id = in.readString();
    this.type = in.readString();
    this.startDate = (Long) in.readValue(Long.class.getClassLoader());
    this.membershipGroupId = in.readString();
    this.planCode = (Long) in.readValue(Long.class.getClassLoader());
    this.bookedEvents = (Integer) in.readValue(Integer.class.getClassLoader());
    this.expiryDate = (Long) in.readValue(Long.class.getClassLoader());
  }

  public static final Parcelable.Creator<Membership> CREATOR =
      new Parcelable.Creator<Membership>() {
        @Override public Membership createFromParcel(Parcel source) {
          return new Membership(source);
        }

        @Override public Membership[] newArray(int size) {
          return new Membership[size];
        }
      };
}
