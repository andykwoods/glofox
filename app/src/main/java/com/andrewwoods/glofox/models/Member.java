package com.andrewwoods.glofox.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class Member implements Parcelable {

  @SerializedName("_id") private String id;
  private Boolean active;
  private String branchId;
  private String email;
  private String firstName;
  private String lastName;
  private String namespace;
  private String phone;
  private String type;
  private String name;
  private String imageUrl;
  private Membership membership;

  public String getId() {
    return id;
  }

  public Boolean getActive() {
    return active;
  }

  public String getBranchId() {
    return branchId;
  }

  public String getEmail() {
    return email;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getNamespace() {
    return namespace;
  }

  public String getPhone() {
    return phone;
  }

  public String getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public Membership getMembership() {
    return membership;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.id);
    dest.writeValue(this.active);
    dest.writeString(this.branchId);
    dest.writeString(this.email);
    dest.writeString(this.firstName);
    dest.writeString(this.lastName);
    dest.writeString(this.namespace);
    dest.writeString(this.phone);
    dest.writeString(this.type);
    dest.writeString(this.name);
    dest.writeString(this.imageUrl);
    dest.writeParcelable(this.membership, flags);
  }

  public Member() {
  }

  protected Member(Parcel in) {
    this.id = in.readString();
    this.active = (Boolean) in.readValue(Boolean.class.getClassLoader());
    this.branchId = in.readString();
    this.email = in.readString();
    this.firstName = in.readString();
    this.lastName = in.readString();
    this.namespace = in.readString();
    this.phone = in.readString();
    this.type = in.readString();
    this.name = in.readString();
    this.imageUrl = in.readString();
    this.membership = in.readParcelable(Membership.class.getClassLoader());
  }

  public static final Parcelable.Creator<Member> CREATOR = new Parcelable.Creator<Member>() {
    @Override public Member createFromParcel(Parcel source) {
      return new Member(source);
    }

    @Override public Member[] newArray(int size) {
      return new Member[size];
    }
  };
}
