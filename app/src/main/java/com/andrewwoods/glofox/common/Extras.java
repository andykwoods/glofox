package com.andrewwoods.glofox.common;

public class Extras {
  public static final String REQUEST_PAGE = "REQUEST_PAGE";
  public static final String RESULT_RECEIVER = "RESULT_RECEIVER";
  public static final String RESPONSE_CODE = "RESPONSE_CODE";
  public static final String PARAM_MEMBERS_RESPONSE = "PARAM_MEMBERS_RESPONSE";

}
